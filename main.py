points = {"e": 1, "a": 1, "i": 1, "o": 1, "n": 1, "r": 1, "t": 1, "l": 1, "s": 1, "u": 1,
          "d": 2, "g": 2, "b": 3, "c": 3, "m": 3, "p": 3, "f": 4, "h": 4, "v": 4, "w": 4, "y": 4,
          "k": 5, "j": 8, "x": 8, "q": 10, "z": 10}


def scrabble(text):
    print(sum(points[letter] for letter in text))
    """point = 0
    for letter in text.lower():
        point += points[letter]
    print(f"{text} has {point} points")"""


if __name__ == "__main__":
    while True:
        words = input("\nEnter a word: ")
        scrabble(words)
        agree = input("\nDo you want play? Y/N")
        if agree == "N" or agree == "n":
            break
